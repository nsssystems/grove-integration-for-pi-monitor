# README #

The sampleSensors.py program continuously samples sensors attached to the GrovePi board and writes any valid readings to flat files. The program is based extensively on python samples in the GrovePi Python Software library.

### Set Up ###

You must install the GrovePi controller board and relevant SeedStudio sensors before using this software. The build list to set up this unit is here:

https://docs.google.com/a/umass.edu/spreadsheets/d/1kRNqOQDu5o6FQmJlJ4hLEiaramkt2Rpy4IPYWhXu4HQ/edit?usp=sharing

After installing the hardware, you must install the GrovePi python module:

    sudo apt-get install python-setuptools
    git clone https://github.com/DexterInd/GrovePi.git
    cd GrovePi/Software/Python
    sudo python setup.py install

Now update GrovePi firmware to version 1.2.5:

    cd ../../Firmware/Source/v1.2/grove_pi_v1_2_5/
    avrdude -c gpio -p m328p -U flash:w:grove_pi_v1_2_5.cpp.hex

You must create the directory for the program to write files:

    sudo mkdir /var/sensors
    sudo chown pi:pi /var/sensors

You should now be able to run the sampleSensors.py program to sample the sensors. The program should output sensor values until you press ctrl-c. Note that while manual execution is supported, this program is designed to be run as a service through systemd.


### Contact Information ###

* This is a sample program developed by the UMass Enterprise Applications team.
* for more information, contact [Mark Scarbrough](marks@umass.edu)