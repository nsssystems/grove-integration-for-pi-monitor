#!/usr/bin/env python

import grovepi
import os
import math
import sys
import time
import atexit
import syslog

# Enable sensor and register disable function
grovepi.dust_sensor_en()
atexit.register(grovepi.dust_sensor_dis)

def writeFile(name,data):
    sensorFile = '/var/sensors/' + name
    newSensorFile = sensorFile + '.new'
    f = open(newSensorFile, 'w')
    f.write(str(data))
    f.flush()
    os.fsync(f.fileno())
    f.close()
    os.rename(newSensorFile, sensorFile)

def log(message):
    if sys.stdout.isatty():
        print  message
    syslog.syslog(message)

        
def trySensor(readFunction):
    
    sensor = readFunction.__name__
    
    try:
        value = readFunction()
    except TypeError:
        log('TypeError while executing ' + sensor)
    except IOError:
        log('IOError while executing ' + sensor)
    else:
        log('Read value from sensor ' + sensor + ': ' + str(value))

def dust():

    [new_val,lowpulseoccupancy] = grovepi.dustSensorRead()

    # Guard against invalid reading
    if math.isnan(lowpulseoccupancy):
        raise IOError("Invalid sensor reading.")

    # Only register valid readings
    if new_val:
        # Write dust file
        writeFile('dust',lowpulseoccupancy)
        # return new value
        return lowpulseoccupancy
    else:
        # return something to log
        return 'No new value'
        
   


def dht():
    # The digital port used on the GrovePI
    port = 4  # The Sensor goes on digital port 4.

    # Sensor type, 0 for blue, 1 for white (pro)
    model = 1    # The white colored sensor.

    [temperature,humidity] = grovepi.dht(port,model)

    # Guard against invalid reading
    if math.isnan(temperature) or math.isnan(humidity):
        raise IOError("Invalid sensor reading.")

    # Write files
    writeFile('temperature',temperature)
    writeFile('humidity',humidity)
    
    # Return values
    return str(humidity) + '/' + str(temperature)


def gas():
    gas_sensor = 0
    grovepi.pinMode(gas_sensor,"INPUT")
    gas = grovepi.analogRead(gas_sensor)

    # Interpret value
    gas = (float)(gas / 1024)

    # Write files
    writeFile('gas',gas)
    
    # Return values
    return gas

def flame():
    flame_sensor = 3
    grovepi.pinMode(flame_sensor,"INPUT")
    flame = grovepi.digitalRead(flame_sensor)
    
       # Interpret value
    flame = (flame -1) * -1
    
    # Write files
    writeFile('flame',flame)
    
    # Return values
    return flame
    

def water():
    water_sensor = 5
    grovepi.pinMode(water_sensor,"INPUT")
    water = grovepi.digitalRead(water_sensor)

    # Interpret value
    water = (water - 1) * -1

    # Write files
    writeFile('water',water)
    
    # Return values
    return water

while True:
    try:
        trySensor(dust)
        trySensor(dht)
        trySensor(gas)
        trySensor(flame)
        trySensor(water)
        time.sleep(5)
    except KeyboardInterrupt:
        sys.exit(0)
    except Exception as e:
        log ('Unhandled exception' + str(e))
